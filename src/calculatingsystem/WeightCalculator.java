package calculatingsystem;


import elementsrepository.ChemicalElement;
import elementsrepository.CustomShape;
import elementsrepository.InvalidShapeException;

public class WeightCalculator {
    private ChemicalElement chemicalElement;
    private CustomShape customShape;

    public WeightCalculator() {
    }
    
    public WeightCalculator(ChemicalElement chemicalElement, CustomShape customShape) {
        this.chemicalElement = chemicalElement;
        this.customShape = customShape;
    }

    public double calcWeight(ChemicalElement chemicalElement, CustomShape customShape) throws InvalidShapeException {
        if ((customShape.getLength() < 0) || (customShape.getHeight() < 0) || (customShape.getWidth() < 0)) {
            throw new InvalidShapeException("Shape must be bigger then zero");
        }
        double result = customShape.getHeight() * customShape.getLength() * customShape.getWidth();
        return (chemicalElement.getDensity() * result) / 1000;
    }
}
