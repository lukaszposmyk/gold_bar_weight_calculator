package elementsrepository;

public class InvalidShapeException extends RuntimeException {
    public InvalidShapeException(String message) {
        super(message); }
}
