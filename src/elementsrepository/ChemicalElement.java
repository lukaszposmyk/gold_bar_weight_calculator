package elementsrepository;

public class ChemicalElement {
    private double density;
    private String appearance;
    private double meltingPoint;
    private double boilingPoint;

    public ChemicalElement(double density, String appearance, double meltingPoint, double boilingPoint) {
        this.density = density;
        this.appearance = appearance;
        this.meltingPoint = meltingPoint;
        this.boilingPoint = boilingPoint;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public double getMeltingPoint() {
        return meltingPoint;
    }

    public void setMeltingPoint(double meltingPoint) {
        this.meltingPoint = meltingPoint;
    }

    public double  getBoilingPoint() {
        return boilingPoint;
    }

    public void setBoilingPoint(double boilingPoint) {
        this.boilingPoint = boilingPoint;
    }
}
