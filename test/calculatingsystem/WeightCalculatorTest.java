/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatingsystem;

import elementsrepository.ChemicalElement;
import elementsrepository.CustomShape;
import elementsrepository.InvalidShapeException;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 * @author lukas
 */
public class WeightCalculatorTest {
    
    @Test
    public void method_should_return_zero(){
        
        //given
        WeightCalculator weightCalculator = new WeightCalculator();
        ChemicalElement chemicalElementMock = mock(ChemicalElement.class);
        when(chemicalElementMock.getDensity()).thenReturn(19.3);
        CustomShape customShapeMock = mock(CustomShape.class);
        when(customShapeMock.getHeight()).thenReturn(0.0);
        when(customShapeMock.getLength()).thenReturn(0.0);
        when(customShapeMock.getWidth()).thenReturn(0.0);
        
        //when
        double result = weightCalculator.calcWeight(chemicalElementMock, 
                customShapeMock);
        
        //then
        assertEquals(0,result,0);
    }
    
    @Test
    public void method_should_return_zero_coma_one_five_four_four() {
        
        //given
        WeightCalculator weightCalculator = new WeightCalculator();
        ChemicalElement chemicalElementMock = mock(ChemicalElement.class);
        when(chemicalElementMock.getDensity()).thenReturn(19.3);
        CustomShape customShapeMock = mock(CustomShape.class);
        when(customShapeMock.getHeight()).thenReturn(2.0);
        when(customShapeMock.getLength()).thenReturn(2.0);
        when(customShapeMock.getWidth()).thenReturn(2.0);
        
        //when
        double result = weightCalculator.calcWeight(chemicalElementMock, 
                customShapeMock);
        
        //then
        assertEquals(0.1544, result, 0);
        }
    
    @Test(expected = InvalidShapeException.class)
    public void method_should_return_exception_when_one_argument_below_zero(){
        
        //given
        WeightCalculator weightCalculator = new WeightCalculator();
        ChemicalElement chemicalElementMock = mock(ChemicalElement.class);
        when(chemicalElementMock.getDensity()).thenReturn(19.3);
        CustomShape customShapeMock = mock(CustomShape.class);
        when(customShapeMock.getHeight()).thenReturn(-5.0);
        when(customShapeMock.getLength()).thenReturn(5.0);
        when(customShapeMock.getWidth()).thenReturn(5.0);
        
        //when
        double result = weightCalculator.calcWeight(chemicalElementMock,
                customShapeMock);
        
        //then
        fail();
    }
    
     @Test(expected = InvalidShapeException.class)
    public void method_should_return_exception_when_two_argument_below_zero(){
        
        //given
        WeightCalculator weightCalculator = new WeightCalculator();
        ChemicalElement chemicalElementMock = mock(ChemicalElement.class);
        when(chemicalElementMock.getDensity()).thenReturn(19.3);
        CustomShape customShapeMock = mock(CustomShape.class);
        when(customShapeMock.getHeight()).thenReturn(-5.0);
        when(customShapeMock.getLength()).thenReturn(-5.0);
        when(customShapeMock.getWidth()).thenReturn(5.0);
        
        //when
        double result = weightCalculator.calcWeight(chemicalElementMock,
                customShapeMock);
        
        //then
        fail();
    }
}
